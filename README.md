# Mathematical Statements

## Description

In this application, the user is asked to enter different mathematical statements, and the application will compute it. The allowed operations are as follows:

. plus
- Example: 
Input: "two plus three"
Output: 5
. minus
- Example:
Input: "five minus one"
Output: 4
. times
- Example:
Input: "three times four"
Output: 12
. divided by
Input: "2 divided by 5"
Output: 0.4

The numbers that are allowed as input are every number between zero and one thousand, except eleven, twelve, thirteen, forteen, fifteen, sixteen, seventeen, eighteen and nineteen.